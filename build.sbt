name := "solar-sytem"

version := "0.1"

scalaVersion := "2.13.2"

scalacOptions ++= Seq("-deprecation")

libraryDependencies += "org.processing" % "core" % "3.3.7"
