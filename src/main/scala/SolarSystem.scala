import models.Planet
import peasy.PeasyCam
import processing.core.{PApplet, PConstants}

class SolarSystem extends PApplet {
  implicit private val applet: PApplet = this

  var cam: PeasyCam = _
  var sun: Planet = _

  override def settings(): Unit = size(600, 600, PConstants.P3D)

  override def setup(): Unit = {
    cam = new PeasyCam(this, 500)
    cam.setWheelScale(0.03)
    cam.setMaximumDistance(800)
    cam.setMinimumDistance(100)

    sun = Planet(50, 0, 0)
    sun.spawnMoons(5)
  }

  override def draw(): Unit = {
    background(0)
    pointLight(255, 255, 255, 0, 0, 0)

    sun.show()
    sun.orbit()
  }

  override def mouseClicked(): Unit = {
    frameCount = -1
    noiseSeed((random(1)*1000).toLong)
  }
}

object SolarSystem extends App {
  PApplet.main("SolarSystem")
}
