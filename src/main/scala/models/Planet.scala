package models

import processing.core.{PApplet, PVector}
import processing.core.PConstants.TWO_PI

case class Planet(radius: Float, distance: Float, velocity: Float, level: Int = 1)(implicit applet: PApplet) {
  import applet._

  private val position: PVector = PVector.random3D.mult(distance)
  private val normal: PVector = position.cross(PVector.random3D).setMag(0.75f * distance)

  var angle: Float = random(TWO_PI)
  var planets: Seq[Planet] = Seq.empty

  def spawnMoons(number: Int): Unit = {
    planets = (1 to number).map { _ =>
      val childRadius = radius / (2 * level)
      val childDistance = random(radius + childRadius, (radius + childRadius) * 2)
      val childVelocity = random(-0.6f, 0.6f).toRadians
      Planet(childRadius, childDistance, childVelocity, level + 1)
    }

    if  (level < 2)
      planets.foreach(_.spawnMoons(random(0, 4).toInt))
  }

  def orbit(): Unit = {
    angle += velocity
    planets.foreach(_.orbit())
  }

  def show(): Unit = {
    pushMatrix()

    rotate(angle, normal.x, normal.y, normal.z)
    translate(position.x, position.y, position.z)

    noStroke()
    fill(getColor)
    sphere(radius)
    //ellipse(0, 0, radius * 2, radius * 2)

    planets.foreach(_.show())

    popMatrix()
  }

  private def getColor: Int = {
    val r = noise(level * 1000f) * 255 + 50
    val g = noise(level * 2000f) * 255 + 50
    val b = noise(level * 3000f) * 255 + 50

    0xFF000000 +
      r.toInt * 0x10000 +
      g.toInt * 0x100 +
      b.toInt
  }
}
